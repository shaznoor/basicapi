package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CustomerDto;
import com.example.demo.dto.CustomerRequestDto;
import com.example.demo.entity.Customer;
import com.example.demo.repository.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
	private CustomerRepository customerRepository;
	public List<CustomerDto> getCustomer(){
		List<Customer> cust= customerRepository.findAll();
		List<CustomerDto> cust1=new ArrayList<>();
		for (Customer customer : cust) {
			CustomerDto a=new CustomerDto();
			BeanUtils.copyProperties(customer, a);
			//a.setId(customer.getId());
			//a.setName(customer.getName());
			cust1.add(a);
		}
		return cust1;
	}
	
	public CustomerDto getSingleCustomer(Long id) {
		Optional<Customer> cus=customerRepository.findById(id);
		CustomerDto cust=new CustomerDto();
		cust.setId(cus.get().getId());
		cust.setName(cus.get().getName());
		return cust;
	}
	
	public CustomerDto saveCustomer(CustomerRequestDto name) {
		Customer cus=new Customer();
		cus.setName(name.getName());
		Customer saved=customerRepository.save(cus);
		CustomerDto cust=new CustomerDto();
		cust.setId(saved.getId());
		cust.setName(saved.getName());
		return cust;
	}
	
	public CustomerDto removeCustomer(Long id) {
		customerRepository.deleteById(id);
		CustomerDto cust=new CustomerDto();
		cust.setId(null);
		cust.setName("");
		return cust;
	}
}
