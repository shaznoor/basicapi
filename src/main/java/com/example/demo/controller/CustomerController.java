package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CustomerDto;
import com.example.demo.dto.CustomerRequestDto;
import com.example.demo.service.CustomerService;

@RestController
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	@GetMapping("/customer")
	public List<CustomerDto> getCustomer() {
		return customerService.getCustomer();
	}
	@GetMapping("/customer/{id}")
	public CustomerDto getSingleCustomer(@PathVariable Long id) {
		return customerService.getSingleCustomer(id);
	}
	@PostMapping("/addcustomer")
	public CustomerDto addCustomer(@RequestBody CustomerRequestDto name) {
		return customerService.saveCustomer(name);
	}
	@DeleteMapping("/delete/{id}")
	public CustomerDto deleteCustomer(@PathVariable Long id) {
		return customerService.removeCustomer(id);
	}
}
