package com.example.demo.dto;

public class CustomerRequestDto {
	private String name;
	
	public CustomerRequestDto(String name) {
		this.name = name;
	}
	public CustomerRequestDto() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
